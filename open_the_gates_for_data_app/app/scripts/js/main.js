
/***********************************************************************************
*	Variables
***********************************************************************************/
var events;

/***********************************************************************************
*	Init
***********************************************************************************/
$(document).ready(init);

function init(e) 
{
	$.getJSON("http://datatank.stad.gent/4/toerisme/gentsefeestenevents.json", handleEvents);
	$.getJSON("http://datatank.stad.gent/4/toerisme/gentsefeestencategorien.json", handleCategories);
	$.getJSON("http://datatank.stad.gent/4/cultuursportvrijetijd/gentsefeestenlocaties.json", handleLocations);
	$.getJSON("http://datatank.stad.gent/4/cultuursportvrijetijd/gentsefeestendata.json", handleDates);
}

/***********************************************************************************
*	Methods
***********************************************************************************/
function handleEvents(data)
{
	events = data;
	$.each(data, function(key, val) {
		var div = document.createElement('div');
		$(div).addClass('country__listitem');
		$(div).addClass('cat-' + val.categorie_id);
		$(div).addClass('loc-' + val.locatie_id);
		$(div).addClass('dat-' + val.datum);
		
		var title = document.createElement('h3');
		$(title).html(val.titel);
		$(div).append(title);
		
		var category = document.createElement('h3');
		$(category).html(val.categorie_naam);
		$(div).append(category);
        var button = document.createElement("BUTTON").name='h3';
		
		var locatie = document.createElement('h3');
		$(locatie).html(val.locatie);
		$(div).append(locatie);
        var button = document.createElement("BUTTON");
		
		var date = document.createElement('h3');
		var t = new Date(val.datum*1000);
		var dd = t.getDate();
		var mm = t.getMonth() + 1;
		var yyyy = t.getFullYear();
		$(date).html(dd + '/' + mm + '/' + yyyy);
		$(div).append(date);
        var button = document.createElement("BUTTON");
		
		$('.countries-list').append(div);
	});
}

function handleCategories(data)
{
	$.each(data, function(key, val) {
		var li = createListItem('cat-' + val.id, val.title, '.nav-cats');
	});
}

function handleLocations(data)
{
	$.each(data, function(key, val) {
		createListItem('loc-' + val.id, val.naam, '.nav-locs');
	});
}

function handleDates(data)
{
	$.each(data, function(key, val) {
		createListItem('dat-' + val.timestamp, val.day, '.nav-dates');
	});
}

function createListItem(id, title, container_id)
{
	var li = document.createElement('li');
	$(li).addClass(id);
	$(li).html(title);
	$(li).click(loadItems);
		
	$(container_id).append(li);
}

function loadItems(e) {
	var cls = $(e.currentTarget).attr('class');
	
	$('.' + cls).fadeIn();
	$('.country__listitem').not('.' + cls).fadeOut();
}



// Anonymous function executed when document loaded
(function() {

    // Describe an App object with own functionalities
    var App = {
        init: function() {
            var self = this;


            // Handlebars Cache
            this._hbsCache = {};// Handlebars cache for templates
            this._hbsPartialsCache = {};// Handlebars cache for partials

            // Create a clone from the JayWalker object
            this._jayWalker = JayWalker;
            this._jayWalker.init();





            this.registerNavigationToggleListeners();// Register All Navigation Toggle Listeners

            this.registerWindowListeners();// Register All Navigation Toggle Listeners



        },

        loadCountryInfo: function() {
            // Closure
            var self = this, url = String.format(this.COUNTRYINFO);

            // Load JSONP from corresponding API with certain URL
            // JSONP Callback is defined by a function name in this case
            // prefix=jsonp_callback. The Utils object contains a new function
            // which can handle the callback
            Utils.getJSONByPromise(url).then(
                function(data) {
                    if(data != null) {
                        self._dataCountry.extrainfo = data;
                    }

                },

                function(status) {
                    console.log(status);
                }
            );
        },

        registerNavigationToggleListeners: function() {
            var toggles = document.querySelectorAll('.navigation-toggle');

            if(toggles != null && toggles.length > 0) {
                var toggle = null;

                for(var i = 0; i < toggles.length; i++ ) {
                    toggle = toggles[i];
                    toggle.addEventListener('click', function(ev) {
                        ev.preventDefault();

                        document.querySelector('body').classList.toggle(this.dataset.navtype);

                        return false;
                    });
                }
            }
        },
        registerWindowListeners: function() {
            window.addEventListener('resize', function(ev) {
                if(document.querySelector('body').classList.contains('offcanvas-open')) {
                    document.querySelector('body').classList.remove('offcanvas-open');
                }

                if(document.querySelector('body').classList.contains('headernav-open')) {
                    document.querySelector('body').classList.remove('headernav-open');
                }
            });
        }
    };

    App.init();// Intialize the application
    //functie voor de zoekbar
    document.querySelector('#txtSearch').addEventListener('keyup', function(ev)           {
        ev.preventDefault();

        var txt = ev.target.value.toLowerCase();

        var countryNodes = document.querySelectorAll('.countries-list>ul>li');
        for(var i=0;i<countryNodes.length;i++) {
            var c = countryNodes[i];
            if(c.dataset.title.toLowerCase().indexOf(txt) == -1) {
                c.classList.add('countryHidden');
            } else {
                c.classList.remove('countryHidden');
            }
        }

        return false;
    });

})();





// QUIZ



(function() {
    var questions = [{
        question: "How long does the Gentse Feesten take?",
        choices: [" 7 days"," 10 days"," 12 days" ," 14 days", " 16 days"],
        correctAnswer: 1
    }, {
        question: "In which year was the first edition of the Gentse Feesten?",
        choices: [" 1921"," 1831"," 1989"," 1878"," 1843"],
        correctAnswer: 4
    }, {
        question: "What was the original location of the Gentse Feesten?",
        choices: [" Drongen"," Wondelgem", " Sint-Denijs-Westrem"," Gentbrugge"," Sint-Martens-Latem"],
        correctAnswer: 2
    }, {
        question: "If you know the Gentse Feesten, you know Polé Polé but what does Polé Polé mean?",
        choices: [" PARTYYY"," It's a music style from Africa", " Let's take it easy"," Let's dance", " Shake it"],
        correctAnswer: 2
    }, {
        question: "After being moved to Gent itself, where did the Gentse Feesten began?",
        choices: [" Sint-Jacobs"," Kouter"," Graslei"," Korenmarkt"," Vrijdagsmarkt"],
        correctAnswer: 0
    }];

    var questionCounter = 0; //Tracks question number
    var selections = []; //Array containing user choices
    var quiz = $('#quiz'); //Quiz div object

    // Display initial question
    displayNext();

    // Click handler for the 'next' button
    $('#next').on('click', function (e) {
        e.preventDefault();

        // Suspend click listener during fade animation
        if(quiz.is(':animated')) {
            return false;
        }
        choose();

        // If no user selection, progress is stopped
        if (isNaN(selections[questionCounter])) {
            alert('You have to choose something!');
        } else {
            questionCounter++;
            displayNext();
        }
    });

    // Click handler for the 'prev' button
    $('#prev').on('click', function (e) {
        e.preventDefault();

        if(quiz.is(':animated')) {
            return false;
        }
        choose();
        questionCounter--;
        displayNext();
    });

    // Click handler for the 'Start Over' button
    $('#start').on('click', function (e) {
        e.preventDefault();

        if(quiz.is(':animated')) {
            return false;
        }
        questionCounter = 0;
        selections = [];
        displayNext();
        $('#start').hide();
    });

    // Animates buttons on hover
    $('.button').on('mouseenter', function () {
        $(this).addClass('active');
    });
    $('.button').on('mouseleave', function () {
        $(this).removeClass('active');
    });

    // Creates and returns the div that contains the questions and
    // the answer selections
    function createQuestionElement(index) {
        var qElement = $('<div>', {
            id: 'question'
        });

        var header = $('<h2>Question ' + (index + 1) + ':</h2>');
        qElement.append(header);

        var question = $('<p>').append(questions[index].question);
        qElement.append(question);

        var radioButtons = createRadios(index);
        qElement.append(radioButtons);

        return qElement;
    }

    // Creates a list of the answer choices as radio inputs
    function createRadios(index) {
        var radioList = $('<ul>');
        var item;
        var input = '';
        for (var i = 0; i < questions[index].choices.length; i++) {
            item = $('<li>');
            input = '<input type="radio" name="answer" value=' + i + ' />';
            input += questions[index].choices[i];
            item.append(input);
            radioList.append(item);
        }
        return radioList;
    }

    // Reads the user selection and pushes the value to an array
    function choose() {
        selections[questionCounter] = +$('input[name="answer"]:checked').val();
    }

    // Displays next requested element
    function displayNext() {
        quiz.fadeOut(function() {
            $('#question').remove();

            if(questionCounter < questions.length){
                var nextQuestion = createQuestionElement(questionCounter);
                quiz.append(nextQuestion).fadeIn();
                if (!(isNaN(selections[questionCounter]))) {
                    $('input[value='+selections[questionCounter]+']').prop('checked', true);
                }

                // Controls display of 'prev' button
                if(questionCounter === 1){
                    $('#prev').show();
                } else if(questionCounter === 0){

                    $('#prev').hide();
                    $('#next').show();
                }
            }else {
                var scoreElem = displayScore();
                quiz.append(scoreElem).fadeIn();
                $('#next').hide();
                $('#prev').hide();
                $('#start').show();
            }
        });
    }

    // Computes score and returns a paragraph element to be displayed
    function displayScore() {
        var score = $('<p>',{id: 'question'});

        var numCorrect = 0;
        for (var i = 0; i < selections.length; i++) {
            if (selections[i] === questions[i].correctAnswer) {
                numCorrect++;
            }
        }

        score.append('You got ' + numCorrect + ' questions out of ' +
            questions.length + ' right!');
        return score;
    }
})();










